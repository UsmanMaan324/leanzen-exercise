import pytest
from src.customer import Customer, Customers
from src.booking import RentalInfo, Booking
from datetime import datetime


def test_init_customer_happy_path():
    customer = Customer("Usman", "0000", "test@gmail.com")
    assert customer.__class__.__name__ == "Customer"
    assert customer.get_customer_name() == "Usman"
    assert customer.get_phone_number() == "0000"
    assert customer.get_email() == "test@gmail.com"


def test_init_customer_not_happy_path():
    customer = Customer("Usman", "0000", "test@gmail.com")
    assert customer.__class__.__name__ != "Dummy"
    assert customer.get_customer_name() != "Usman Maan"
    assert customer.get_phone_number() == "0000"
    assert customer.get_email() == "test@gmail.com"


def test_getter_method_happy_path():
    customer = Customer("John", "1111", "john@gmail.com")
    assert customer.get_customer_name() == "John"
    assert customer.get_phone_number() == "1111"
    assert customer.get_email() == "john@gmail.com"


def test_setter_method_happy_path():
    customer = Customer("John", "1111", "john@gmail.com")
    customer.set_customer_name("Charles")
    customer.set_phone_number("3333")
    customer.set_email("charles@gmail.com")
    assert customer.get_customer_name() == "Charles"
    assert customer.get_phone_number() == "3333"
    assert customer.get_email() == "charles@gmail.com"


def test_all_set_method():
    customer = Customer("John", "1111", "john@gmail.com")
    customer.set_all_params("John Grey", "1111", "john.grey@gmail.com")

    assert customer.get_customer_name() == "John Grey"
    assert customer.get_phone_number() == "1111"
    assert customer.get_email() == "john.grey@gmail.com"


def test_create_customer():
    customer = Customer.create_customer("Patrick", "4444", "patrick@gmail.com")

    assert customer.__class__.__name__ == "Customer"
    assert customer.get_customer_name() == "Patrick"
    assert customer.get_phone_number() == "4444"
    assert customer.get_email() == "patrick@gmail.com"


def test_add_customer_in_customers_list():
    customers = Customers()
    customers.add_customer("Patrick", "4444", "patrick@gmail.com")
    customers.add_customer("John", "1111", "john@gmail.com")
    customers_name_list = customers.get_customers_name_list()

    assert customers.__class__.__name__ == "Customers"
    assert len(customers_name_list) == 2
    assert customers_name_list[0] == "Patrick"
    assert customers_name_list[1] == "John"


def test_init_rental_info():
    rental_info = RentalInfo("Usman", datetime.now(), "bikes")

    assert rental_info.__class__.__name__ == "RentalInfo"


def test_add_customer_in_booking(monkeypatch):
    b = Booking()
    inputs = iter(['Usman', '+923048851324', 'test@gmail.com'])
    monkeypatch.setattr('builtins.input', lambda msg: next(inputs))
    b.add_customer()
    customer_name_list = b.get_customers()

    assert len(customer_name_list) == 3


