# leanzen-exercise

## Rent a Vehicle
Choose a self-explaining name for your project.

## Description
It is a console base application to digitize the small startup for vehicle rental company

## Usage
There are two ways to run this application. The first one is to run with an IDE and second one is to run on terminal.

First clone this repo from gitlab by using the following command.
    
    git clone https://gitlab.com/UsmanMaan324/leanzen-exercise.git

**i)** **BY using PyCharm**

After cloning this repo open this project in pycharm and configure your interpreter with main.py file. When you are done with your interpreter configuration, create a separate environment by writing the following command.

    python3 -m venv env_name

Now you need to activate your environment by typing the following command.

    source env_name/bin/activate

The only thing that is left now is to install the dependencies that are listed in requirement.txt file. Use the following command to do this.

    pip install -r requirments.txt

You are done with all your configuration and now click the run button and your application is in running form.

**i) By Using Terminal**

Navigate to your repo folder, create the virtual environment, activate it and install the dependencies. After doing this, write the following command.

    python3 main.py

To run the pytest, write the following command in your project root directory.

    pytest