
class Customer:
    """
    Class to store single customer
    """

    def __init__(self, customer_name: str, phone_number: str, email: str):
        """
        Use to initialise the customer object
        :param customer_name: Name of the customer
        :param phone_number: Phone number of the customer
        :param email: Email of the customer
        """
        self.__customer_name = customer_name
        self.__phone_number = phone_number
        self.__email = email

    def get_customer_name(self):
        """
        Getter method for customer name
        :return: Name of customer
        """
        return self.__customer_name

    def get_phone_number(self):
        """
        Getter method for phone number
        :return: Phone number of customer
        """
        return self.__phone_number

    def get_email(self):
        """
        Getter method for customer email
        :return: Email of customer
        """
        return self.__email

    def set_customer_name(self, customer_name: str):
        """
        Getter method for customer name
        :return: Name of customer
        """
        self.__customer_name = customer_name

    def set_phone_number(self, phone_number: str):
        """
        Getter method for phone number
        :return: Phone number of customer
        """
        self.__phone_number = phone_number

    def set_email(self, email: str):
        """
        Getter method for customer email
        :return: Email of customer
        """
        self.__email = email

    def set_all_params(self, customer_name: str = None, phone_number: str = None, email: str = None):
        """
        Method to set all params of class
        :param customer_name: Name of the customer
        :param phone_number: Phone number of the customer
        :param email: Email of the custom
        """
        if customer_name:
            self.set_customer_name(customer_name)
        if phone_number:
            self.set_phone_number(phone_number)
        if email:
            self.set_email(email)

    @classmethod
    def create_customer(cls, customer_name: str, phone_number: str, email: str):
        """
        Method to create customer object
        :param customer_name: Name of the customer
        :param phone_number: Phone number of the customer
        :param email: Email of the custom
        :return: Customer object
        """
        return cls(customer_name, phone_number, email)

    def display_customer(self):
        """
        Method to display customer

        """
        print(self.__customer_name, "  ", self.__phone_number, "  ", self.__email)


class Customers:
    """
    Class to store all customers
    """
    __customers: list = []

    @classmethod
    def add_customer(cls, customer_name: str, phone_number: str, email: str):
        """
        Method to add customer in __customers list
        :param customer_name: Name of the customer
        :param phone_number: Phone number of the customer
        :param email: Email of the custom
        """
        customer = Customer.create_customer(customer_name, phone_number, email)
        cls.__customers.append(customer)

    @classmethod
    def display_customers(cls):
        """
        Method to display list of customer
        :return:
        """

        for customer in cls.__customers:
            customer.display_customer()

    @classmethod
    def get_customers_name_list(cls) -> list:
        """
        Return the name of all customers
        :return: list of customers name
        """
        customer_name_list: list = []
        for customer in cls.__customers:
            customer_name_list.append(customer.get_customer_name())

        return customer_name_list
