from .booking import Booking


def app():
    booking = Booking()
    apis = [booking.add_customer, booking.add_rental_booking, booking.display_customers,
            booking.display_rental_booking_list, booking.display_vehicle_types]
    flag = True
    while flag:
        print("-----------------######Start######----------------")
        print("Enter 1 to add customer!")
        print("Enter 2 to add rental booking!")
        print("Enter 3 to see customer list!")
        print("Enter 4 to see the rental booking list!")
        print("Enter 5 to see the vehicle inventory!")
        print("Enter -1 to exit!")
        print("-----------------#######End#######----------------\n")

        try:
            choice = int(input("Enter the ID of your selected API:  "))
            if 0 < choice < 6:
                apis[choice-1]()
            elif choice == -1:
                flag = False
            else:
                print("Your are entering the wrong ID")
        except Exception as e:
            print(e)
            print("Please enter the numeric value only!")
