import re
import datetime


def validate_name(name: str) -> bool:
    """
    Return true if the name is valid else false
    :param name: name
    :return: True if the name is valid else False
    """
    name_pattern = '[A-Za-z]{2,15}( [A-Za-z]{2,15})?'
    return bool(re.fullmatch(name_pattern, name))


def validate_phone_number(phone_number: str) -> bool:
    """
    Return true if the phone number is valid else false
    :param phone_number: phone number
    :return: True if the phone number is valid else False
    """
    phone_number_pattern = "^\\+?[1-9][0-9]{7,14}$"
    return bool(re.fullmatch(phone_number_pattern, phone_number))


def validate_email(email: str) -> bool:
    """
    Return true if the email is valid else false
    :param email: email
    :return: true if the email is valid else false
    """

    email_pattern = "([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+"
    return bool(re.fullmatch(email_pattern, email))


def validate_return_date(return_date: str) -> bool:
    """
    Return ture if date formate is valid else false
    :param return_date: Return date of vehicle
    :return: Ture if date formate is valid else False
    """
    formate = "%Y-%m-%d"
    try:
        datetime.datetime.strptime(return_date, formate)
        return True
    except ValueError:
        return False



