from datetime import datetime
from .customer import Customers
from .helper import validate_name, validate_phone_number, validate_email, validate_return_date


class RentalInfo:

    def __init__(self, customer_name: str, rental_date: datetime, vehicle_type: str, return_date: datetime = None):
        """
        Initialization the RentalInfo object
        :param customer_name: Name of the customer
        :param rental_date: rental date
        :param vehicle_type: type of vehicle
        :param return_date: return data
        """
        self.__customer_name = customer_name
        self.__rental_date = rental_date
        self.__vehicle_type = vehicle_type
        self.__return_date = return_date

    @classmethod
    def create_rental_info(cls, customer_name: str, rental_date: datetime,
                           vehicle_type: str, return_date: datetime = None):
        """
        Method to create RentalInfo object
        :param customer_name: Name of the customer
        :param rental_date: rental date
        :param vehicle_type: type of vehicle
        :param return_date: return data
        """
        return cls(customer_name, rental_date, vehicle_type, return_date)

    def display_rental_info(self):
        """
        Method to display the rental info

        """

        print(self.__customer_name, "  ", self.__rental_date.strftime("%Y-%m-%d"),
              "  ", self.__return_date.strftime("%Y-%m-%d"), "  ", self.__vehicle_type)


class Booking:
    __rental_details: RentalInfo = []
    __customers: Customers = Customers()
    __vehicles_inventory: dict = {
        "bikes": 2,
        "cycle": 3,
        "car": 1,
        "boat": 2
    }

    @classmethod
    def add_customer(cls):
        """
        Method to add customer

        """
        print("-----------------######Start######----------------")
        print("Please enter following fields to add customers\n")
        while True:
            # Assume that user enter unique customer name. Because use as selection list in add booking.
            customer_name = input("Enter the name of customer:  ")
            if validate_name(customer_name):
                customers_name_list = cls.__customers.get_customers_name_list()
                if customer_name not in customers_name_list:
                    break
                print("The name should be unique!")
            else:
                print("The length of name should be in between 2 and 15. And no character other then Alphabets!")

        while True:
            phone_number = input("Enter the phone number of customer:  ")
            if validate_phone_number(phone_number):
                break
            print("Phone number should be in this +11122233311")

        while True:
            email = input("Enter the email of customer:  ")
            if validate_email(email):
                break
            print("Email should be in valid formate: usman@gmail.com")

        cls.__customers.add_customer(customer_name, phone_number, email)
        print("Customer is added successfully!")
        print("-----------------#######End#######----------------\n")

    @classmethod
    def get_customers(cls):
        """
        Getter method to get customers from booking
        :return: list of name of customer
        """
        return cls.__customers.get_customers_name_list()

    @classmethod
    def display_customers(cls):
        """
        Method to display customers

        """
        print("List of customer")
        print("-----------------######Start######----------------")
        print("Customer Name  ", " Phone Number  ", "Customer Email")
        cls.__customers.display_customers()
        print("-----------------#######End#######----------------\n")

    @classmethod
    def add_rental_booking(cls):
        """
        Method to add rental booking

        """
        print("-----------------######Start######----------------")
        print("Please enter the following fields to add rental booking\n")
        customers_name_list = cls.__customers.get_customers_name_list()
        if len(customers_name_list) == 0:
            print("Please first enter the customer!\n")
            return
        print("List of customer's name")
        print("-----------------######Start######----------------")
        for name in customers_name_list:
            print(name)
        print("-----------------#######End#######----------------\n")

        while True:
            customer_name = input("Enter the name of customer from available customer name list:  ")
            if customer_name in customers_name_list:
                break
            print("Incorrect name: Please select name from available name list")

        print("Vehicle inventory")
        print("-----------------######Start######----------------")
        for vehicle, inventory in cls.__vehicles_inventory.items():
            print(vehicle, ":  ", inventory)
        print("-----------------#######End#######----------------\n")

        while True:
            vehicle_type = input("Enter the vehicle type from available vehicle types:  ")
            check = cls.__vehicles_inventory.get(vehicle_type, None)
            if vehicle_type in cls.__vehicles_inventory:
                if cls.__vehicles_inventory[vehicle_type] > 0:
                    cls.__vehicles_inventory[vehicle_type] -= 1
                    break
                else:
                    print("Selected vehicle type is not available in inventory: Wait till someone return the vehicle")
                    return
            print("Incorrect vehicle type: Please select from available vehicle types")

        while True:
            print("Date formate should be YYYY-MM-DD")
            return_date = input("Enter the return date for vehicle Or "
                                "press enter button if you not want to provide date:  ")
            if len(return_date) == 0:
                return_date = None
                break
            if validate_return_date(return_date):
                try:
                    return_date = datetime.strptime(return_date, "%Y-%m-%d")
                    if datetime.now() < return_date:
                        break
                    else:
                        print("The return date should be the future date. Mean date greater than the current date")
                except Exception as e:
                    print(e)
            else:
                print("This is the incorrect date string format. It should be YYYY-MM-DD")

        cls.__rental_details.append(RentalInfo.create_rental_info(customer_name, datetime.now(),
                                                                  vehicle_type, return_date))
        print("Booking is added successfully!\n")
        print("-----------------#######End#######----------------\n")

    @classmethod
    def display_vehicle_types(cls):
        """
        Display the available vehicle types

        """
        print("Vehicle inventory")
        print("-----------------######Start######----------------")
        for vehicle, inventory in cls.__vehicles_inventory.items():
            print(vehicle, ":  ", inventory)
        print("-----------------#######End#######----------------\n")

    @classmethod
    def display_rental_booking_list(cls):
        """
        Display the list of rental booking

        """
        print("List of rental booking")
        print("-----------------######Start######----------------")
        print("Customer Name", "  ", "Rental Date", "  ", "Return Date", "  ", "Vehicle Type")
        for rental_info in cls.__rental_details:
            rental_info.display_rental_info()
        print("-----------------#######End#######----------------\n")



